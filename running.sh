#!/bin/bash
git pull
a=$(git tag --list | sed 's/deployment-DEV-//' | sort -n | tail -1)
b="deployment-DEV-$a"
echo $b
c=$(git tag --list | sed 's/deployment-TEST-//' | sort -n | tail -1)
d="deployment-TEST-$c"
echo $d
git ls-remote --tags origin|awk '/([0-9])$/ {print ":" $2}' | xargs git push origin
git ls-remote --tags origin|awk '/([a-zA-Z])$/ {print ":" $2}' | xargs git push origin
git ls-remote --tags origin|awk '/([-])$/ {print ":" $2}' | xargs git push origin
git push origin $b
git push origin $d